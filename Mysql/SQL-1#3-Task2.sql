CREATE TABLE Exams(Id integer, Name VARCHAR(15), Subject VARCHAR(20),Marks integer);
INSERT	INTO Exams VALUES(1, 'Jack', 'Maths',75);
INSERT	INTO Exams VALUES(2, 'Bob', 'Maths',62);
INSERT	INTO Exams VALUES(3, 'Amy', 'Maths',88);
INSERT	INTO Exams VALUES(4, 'Meg', 'Maths',70);
INSERT	INTO Exams VALUES(5, 'Jack', 'Eng',84);
INSERT	INTO Exams VALUES(6, 'Bob', 'Eng',68);
INSERT	INTO Exams VALUES(7, 'Amy', 'Eng',80);
INSERT	INTO Exams VALUES(8, 'Meg', 'Eng',90);
INSERT	INTO Exams VALUES(9, 'Jack', 'Geog',72);
INSERT	INTO Exams VALUES(10, 'Bob', 'Geog',70);
INSERT	INTO Exams VALUES(11, 'Amy', 'Geog',74);
INSERT	INTO Exams VALUES(12, 'Meg', 'Geog',64);
INSERT	INTO Exams VALUES(13, 'Jack', 'Sci',92);
INSERT	INTO Exams VALUES(14, 'Bob', 'Sci',56);
INSERT	INTO Exams VALUES(15, 'Amy', 'Sci',88);
INSERT	INTO Exams VALUES(16, 'Meg', 'Sci',78);
commit;
SELECT * FROM Exams;
SELECT Max(Marks) FROM Exams;
SELECT Min(Marks) FROM Exams;
SELECT Max(Marks) FROM Exams Where Name = 'Jack';
SELECT min(Marks) FROM Exams Where Name = 'Amy';
SELECT count(Marks) FROM Exams Where Marks between 60 and 70 and Subject = 'Eng';
SELECT sum(Marks) FROM Exams Where name='Jack';
SELECT sum(Marks) FROM Exams Where name='Meg';
SELECT sum(Marks) FROM Exams Where Subject = 'Maths';
SELECT avg(Marks) FROM Exams Where Subject='Geog';
SELECT avg(Marks) FROM Exams Where name='Bob';

select * from Exams where marks > (select marks from Exams Where name='Bob' and subject = 'Maths');
select * from Exams where marks < (select marks from Exams Where name='Meg' and subject = 'Sci');

select * from Exams where name = 'Amy' and marks = (select min(marks)from Exams Where name='Amy');

select * from Exams where name = 'Meg' and marks > (select avg(marks) from Exams Where name='Meg');
select * from Exams where name = 'Bob' and marks < (select avg(marks) from Exams Where name='Bob');

SELECT id,Name, subject, marks from exams where name='Jack' and marks = (select max(marks) from Exams Where name='Jack');


