/*Task 1.1 */
CREATE TABLE Months(Id	integer, Name VARCHAR(15), Days integer);
INSERT	INTO Months VALUES(1,'January',31);
INSERT	INTO Months VALUES(2,'February',28);
INSERT	INTO Months VALUES(3,'March',31);
INSERT	INTO Months VALUES(4,'April',30);
INSERT	INTO Months VALUES(5,'May',31);
INSERT	INTO Months VALUES(6,'June',30);
INSERT	INTO Months VALUES(7,'July',31);
INSERT	INTO Months VALUES(8,'August',31);
INSERT	INTO Months VALUES(9,'September',30);
INSERT	INTO Months VALUES(10,'October',31);
INSERT	INTO Months VALUES(11,'November',30);
INSERT	INTO Months VALUES(12,'December',31);
commit;
/*Task 1.2 */
SELECT * FROM Months;
SELECT * FROM Months WHERE Name LIKE 'J%';
SELECT * FROM Months WHERE Name LIKE 'N%';
SELECT * FROM Months WHERE Name LIKE 'Ju%';
SELECT * FROM Months WHERE Name LIKE 'Oc%';
SELECT * FROM Months WHERE Name LIKE '_______';
SELECT * FROM Months WHERE Name LIKE '____';
SELECT * FROM Months WHERE Name LIKE '_e%';
SELECT * FROM Months WHERE Name LIKE '%r';
SELECT * FROM Months WHERE Name LIKE '%ber';
SELECT * FROM Months WHERE Name LIKE '%e%';
SELECT * FROM Months WHERE Name LIKE '%e%' AND  Days = 31;
SELECT * FROM Months WHERE Name LIKE '%u%' AND  Days = 30;
SELECT * FROM Months WHERE Name LIKE '____' AND Name LIKE '%u%';

/*Task 2.1 */
CREATE TABLE Bills(Id	integer, Name VARCHAR(15), Purpose VARCHAR(20),Amount integer);
INSERT	INTO Bills VALUES(1, 'Jack', 'Food', 48);
INSERT	INTO Bills VALUES(2, 'Tim',	'Cab',	14);
INSERT	INTO Bills VALUES(3,'Annie','Flight', 155);
INSERT	INTO Bills VALUES(4, 'Annie', 'Hotel', 89);
INSERT	INTO Bills VALUES(5, 'Annie', 'Cab', 42);
INSERT	INTO Bills VALUES(6, 'Jack', 'Mobile',	29);
INSERT	INTO Bills VALUES(7, 'Steve', 'Food',	25);
INSERT	INTO Bills VALUES(8, 'Tim',	'Cab',	27);
INSERT	INTO Bills VALUES(9,'Jack',	'Food',	36);
INSERT	INTO Bills VALUES(10,'Annie', 'Flight',	140);
INSERT	INTO Bills VALUES(11,'Annie','Cab',	24);
commit;

/*Task 2.2 */
SELECT * FROM Bills;
SELECT * FROM Bills Where Name = 'Annie';
SELECT * FROM Bills Where Purpose = 'Food';
SELECT * FROM Bills Where Amount > 30;
SELECT * FROM Bills Where Purpose = 'Flight' AND Name = 'Annie';
SELECT * FROM Bills Where Purpose = 'Food' AND Name = 'Jack';
SELECT * FROM Bills Where Purpose = 'Cab' AND Amount < 30;
SELECT * FROM Bills Where  Amount > 20 AND Amount < 30;
SELECT * FROM Bills Where Purpose = 'Cab' AND Amount > 20 AND Amount < 30;
UPDATE Bills SET Amount = Amount-2 where Purpose = 'Cab';
UPDATE Bills SET Amount = Amount+5 where Purpose = 'Food';
UPDATE Bills SET Name = 'Timothy' where Name = 'Tim';
delete from Bills Where Purpose = 'Food' AND Name = 'Jack';
delete from Bills Where Purpose = 'Hotel' AND Name = 'Annie';
delete from Bills Where Purpose = 'Cab' AND Amount < 30 ANd Amount > 20 ;
SELECT * FROM Bills;



