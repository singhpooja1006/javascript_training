/*---Task 1----*/
CREATE	TABLE	Employee (
EmpId	INTEGER	AUTO_INCREMENT,
EmpName	VARCHAR(30),
PRIMARY	KEY	(EmpId)
);


CREATE	TABLE	Bills (
BillId	INTEGER	AUTO_INCREMENT,
Employee INTEGER,
BillType VARCHAR(20),
Amount INTEGER,
PRIMARY	KEY	(BillId),
FOREIGN KEY (Employee)
REFERENCES Employee (EmpId)
);


INSERT	INTO Employee (EmpName) VALUES('Jack');
INSERT	INTO Employee (EmpName) VALUES('Tim');
INSERT	INTO Employee (EmpName) VALUES('Annie');
INSERT	INTO Employee (EmpName) VALUES('William');
commit;
select * from Employee;


INSERT	INTO Bills (Employee, BillType,	Amount) VALUES( 1,	'Food',	19);
INSERT	INTO Bills (Employee, BillType,	Amount) VALUES( 3,	'Food',	28);
INSERT	INTO Bills (Employee, BillType,	Amount) VALUES( 2,	'Travel',	450);
INSERT	INTO Bills (Employee, BillType,	Amount) VALUES(4,	'Hotel',	784);
INSERT	INTO Bills (Employee, BillType,	Amount) VALUES( 3,	'Taxi',	16);
INSERT	INTO Bills (Employee, BillType,	Amount) VALUES( 5,	'Travel',	266);/*Error: Cannot add or update a child row: a foreign key constraint fails*/
commit;
select * from Bills;

/*---Task 2----*/

CREATE	TABLE	Students (
StudentId	INTEGER	AUTO_INCREMENT,
StudentName	VARCHAR(30),
PRIMARY	KEY	(StudentId)
);

CREATE	TABLE	Courses (
CourseName	VARCHAR(30),
PRIMARY	KEY	(CourseName)
); 

CREATE	TABLE	Enrollments (
EnrollmentId INTEGER	AUTO_INCREMENT,
Student	INTEGER,
Course	Varchar(30),
PRIMARY	KEY	(EnrollmentId),
FOREIGN KEY (Student)
REFERENCES Students (StudentId),
FOREIGN KEY (Course)
REFERENCES Courses (CourseName)
); 

INSERT	INTO Students (StudentName) VALUES('Jack');
INSERT	INTO Students (StudentName) VALUES('Tim');
INSERT	INTO Students (StudentName) VALUES('Annie');
INSERT	INTO Students (StudentName) VALUES('William');
commit;
select * from Students;


INSERT	INTO Courses (CourseName) VALUES('Spring');
INSERT	INTO Courses (CourseName) VALUES('Android');
INSERT	INTO Courses (CourseName) VALUES('Python');
INSERT	INTO Courses (CourseName) VALUES('Java');
commit;
select * from Courses;

INSERT	INTO Enrollments (Student,	Course) VALUES( 1,	'Java');
INSERT	INTO Enrollments (Student,	Course) VALUES( 2,	'Python');
INSERT	INTO Enrollments (Student,	Course) VALUES( 6,	'Angular');
INSERT	INTO Enrollments (Student,	Course) VALUES( 1,	'Java');/*Error: Cannot add or update a child row: a foreign key constraint fails*/
INSERT	INTO Enrollments (Student,	Course) VALUES( 3,	'Selenium');/*Error: Cannot add or update a child row: a foreign key constraint fails*/
commit;
select * from Enrollments;
