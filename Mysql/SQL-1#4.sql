/*Task 1 */
Select * From Bills;
Select * From Bills Order by Amount ASC;
Select * From Bills Order by Amount DESC;
Select * From Bills Order by Purpose ASC;
Select * From Bills Order by name DESC;
Select * From Bills Order by name DESC, Amount DESC;
Select Distinct Name From Bills;
Select Distinct Purpose From Bills;
Select Distinct Name From Bills Order by name ASC;
Select Distinct Purpose From Bills Order by Purpose DESC;
Select * From Bills Limit 4;
Select * From Bills order by amount desc limit 3;
Select * From Bills order by amount limit 4;
/* Task 2 */
CREATE TABLE Products(Id integer, Name VARCHAR(15), Category VARCHAR(20),Quantity integer,Price integer);
INSERT	INTO Products VALUES(1,	'Coke',	'SoftDrink', 200, 4);
INSERT	INTO Products VALUES(2,	'Diet Coke','SoftDrink', 160, 5);
INSERT	INTO Products VALUES(3,	'Coke Zero', 'SoftDrink', 70, 6);
INSERT	INTO Products VALUES(4,	'Pepsi','SoftDrink', 180, 4);
INSERT	INTO Products VALUES(5,	'RedBull',	'SoftDrink', 170, 8);
INSERT	INTO Products VALUES(6,	'Lux',	'Soap', 60,3);
INSERT	INTO Products VALUES(7,	'Dove',	'Soap', 40,5);
INSERT	INTO Products VALUES(8,	'Pears','Soap', 25,6);
INSERT	INTO Products VALUES(9,	'iPhone7', 'Mobile',400, 6);
INSERT	INTO Products VALUES(10,	'iPhone6', 'Mobile',300, 8);
INSERT	INTO Products VALUES(11,	'iPhone7S', 'Mobile',500, 4);
INSERT	INTO Products VALUES(12	,	'iPhone8', 'Mobile',600, 2);
INSERT	INTO Products VALUES(13,	'Pixel', 'Mobile',550, 2);
INSERT	INTO Products VALUES(14, 'Galaxy S9', 'Mobile',600, 1);
commit;
select * from Products;
Select * From Products Order by Price ASC;
Select * From Products Order by Price DESC;
Select * From Products Order by Quantity ASC;
Select * From Products Order by Quantity DESC;
Select * From Products Order by Quantity, Price ;
Select * From Products Order by Quantity * Price ;

Select * From Products where Name like 'iPhone%';
Select * From Products where Name like 'Coke%';
Select distinct Name From Products order by Name;
Select * From Products limit 4;
Select * From Products Order by price desc limit 3;
Select * From Products Order by Quantity  limit 1;
select * from Products where price > (Select max(price) From Products where Category = 'SoftDrink');

select * from Products where Quantity > (Select Quantity From Products where Name = 'Dove');
select * from Products where Quantity > (Select Quantity From Products where Category = 'Soap');

Select * From Products where Category = 'Soap' order by price desc limit 3;

Select * From Products where Category = 'Mobile' order by price limit 2;

Select * From Products where name = 'Dove';

update Products set Quantity = Quantity - 10  where name = 'Dove';
update Products set Quantity = Quantity - 3  where name = 'Pixel';
update Products set Quantity = Quantity + 20  where name = 'RedBull';
update Products set Quantity = Quantity + 10  where name = 'Diet Coke';
update Products set Quantity = Quantity + 2  where name = 'iPhone6';
update Products set Quantity = Quantity + 8  where name = 'Coke Zero';
commit;








