/* Task 1 */
CREATE TABLE Bills(Id	integer, Name VARCHAR(15), Purpose VARCHAR(20),Amount integer);
INSERT	INTO Bills VALUES(1, 'Jack', 'Food', 48);
INSERT	INTO Bills VALUES(2, 'Tim',	'Cab',	14);
INSERT	INTO Bills VALUES(3,'Annie','Flight', 155);
INSERT	INTO Bills VALUES(4, 'Annie', 'Hotel', 89);
INSERT	INTO Bills VALUES(5, 'Annie', 'Cab', 42);
INSERT	INTO Bills VALUES(6, 'Jack', 'Mobile',	29);
INSERT	INTO Bills VALUES(7, 'Steve', 'Food',	25);
INSERT	INTO Bills VALUES(8, 'Tim',	'Cab',	27);
INSERT	INTO Bills VALUES(9,'Jack',	'Food',	36);
INSERT	INTO Bills VALUES(10,'Annie', 'Flight',	140);
INSERT	INTO Bills VALUES(11,'Annie','Cab',	24);
commit;
SELECT * FROM Bills;
SELECT Max(Amount) FROM Bills;
SELECT Min(Amount) FROM Bills;
SELECT Max(Amount) as MaxBill FROM Bills;
SELECT Min(Amount)  as MinBill FROM Bills;
SELECT Count(Amount) FROM Bills Where Amount > 30;
SELECT Count(Purpose) FROM Bills Where Amount > 20 and Purpose='Cab' ;
SELECT Count(*) FROM Bills Where Name='Annie' ;
SELECT Count(*) FROM Bills Where Name='Jack' and   Purpose='Food';
SELECT sum(Amount) FROM Bills;
SELECT sum(Amount) FROM Bills where Purpose='Food';
SELECT sum(Amount) FROM Bills where Purpose='Cab';
SELECT sum(Amount) FROM Bills where Name='Jack';
SELECT * FROM Bills where Amount > (SELECT Max(Amount) FROM Bills where Purpose='Cab');
SELECT * FROM Bills where Amount < (SELECT Max(Amount) FROM Bills where Purpose='Food');
SELECT * FROM Bills where Amount < (SELECT * FROM Bills where Amount = Amount*2);





