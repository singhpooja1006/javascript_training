/*_________Task 1 ___________*/
create table Employees (Id	integer, Name Varchar(15));
Create Table Bills (BillNo Integer,	EmpId	Integer, Purpose	Varchar(20), Amount	Integer);
Insert into Employees Values(1,'Jack');
Insert into Employees Values(2,'Tim');
Insert into Employees Values(3,'Annie');
Insert into Employees Values(4,'Steve');
Insert into Employees Values(5,'Mary');
commit;
select * from Employees;

Insert into Bills Values(104,	1,	'Food',	48);
Insert into Bills Values(211,	2,	'Cab',	14);
Insert into Bills Values(304,	3,	'Flight',	155);
Insert into Bills Values(135,	3,	'Hotel',	89);
Insert into Bills Values(188,	3,	'Cab',	42);
Insert into Bills Values(273,	1,	'Mobile',	29);
Insert into Bills Values(302,	4,	'Food',	25);
Insert into Bills Values(347,	2,	'Cab',	27);
Insert into Bills Values(115,	1,	'Food',	36);
Insert into Bills Values(294,	3,'Flight',	140);
Insert into Bills Values(154,	3,	'Cab',	24);
commit;
select * from Bills;
select BillNo, Name, Purpose, Amount from Bills INNER JOIN Employees ON Bills.EmpId=Employees.Id;
select  Name, Purpose, sum(Amount) from Bills INNER JOIN Employees ON Bills.EmpId=Employees.Id group by Name, Purpose;
select Name, sum(Amount) from Bills INNER JOIN Employees ON Bills.EmpId=Employees.Id group by Name;

/*_________Task 2 ___________*/
Select * From Category;
Select * From Products;
select Id, Name, Category.category, Price, Quantity from Products INNER JOIN Category ON Products.CatId=Category.Id;
select Id, Name, Category.category, Price, Quantity from Products INNER JOIN Category ON Products.CatId=Category.Id
where products.Quantity > 20;
select Id, Name, Category.category, Price, Quantity from Products INNER JOIN Category ON Products.CatId=Category.Id
where products.price < 100;