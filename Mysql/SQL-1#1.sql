INSERT	INTO Product VALUES(1,	'Pepsi',	10,	100);
INSERT	INTO Product VALUES(3,	'Colgate',	6,	50);
INSERT	INTO Product VALUES(5,	'iPhone',	499, 8);
INSERT	INTO Product VALUES(6,	'Snickers',	4,	150);
INSERT	INTO Product VALUES(9,	'Levis',	50,	20);

commit;

select * from product;
select name from product;
select * from product where quantity > 40;
select * from product where name ='iPhone';
select * from product where price < 15;

INSERT	INTO Product VALUES(15,	'Pantene',	5,	60);
INSERT	INTO Product VALUES(18,	'LED Bulb',	12,	30);
INSERT	INTO Product VALUES(22,	'Charger',	20, 10);
commit;

select * from Product;
delete from Product where quantity < 10;
commit;
select * from Product;
delete from Product where name = 'Levis';
commit;
select * from Product;
delete from Product where id > 20;
commit;
select * from Product;
delete from Product where price > 50;
commit;
DROP TABLE Product;


