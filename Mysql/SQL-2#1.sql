/*------Task 1-----------*/
create table Bill(Id integer, Name VARCHAR(15), Purpose VARCHAR(20),Amount integer,	primary	key(id));
INSERT	INTO Bill VALUES(1,	'Jack',	'Food',	48);
INSERT	INTO Bill VALUES(2,	'Tim',	'Cab',	14);
INSERT	INTO Bill VALUES(3,	'Annie',	'Flight',	155); 
INSERT	INTO Bill VALUES(1,	'Annie',	'Hotel',	89); /* Error Code: 1062. Duplicate entry '1' for key 'bill.PRIMARY'*/
INSERT	INTO Bill VALUES(4,	'Annie',	'Cab',	42);
INSERT	INTO Bill VALUES(2,	'Jack',	'Mobile',	29); /* Error Code: 1062. Duplicate entry '2' for key 'bill.PRIMARY'*/


/*------Task 2-----------*/
create table MyBills(Id integer auto_increment, Name VARCHAR(15), Purpose VARCHAR(20),Amount integer,	primary	key(id));
INSERT	INTO MyBills (Name,Purpose,Amount) VALUES('Jack',	'Food',	48);
INSERT	INTO MyBills (Name,Purpose,Amount) VALUES('Tim',	'Cab',	14);
INSERT	INTO MyBills (Name,Purpose,Amount) VALUES('Annie',	'Flight',	155); 
INSERT	INTO MyBills (Name,Purpose,Amount) VALUES(	'Annie',	'Hotel',	89); 
INSERT	INTO MyBills (Name,Purpose,Amount) VALUES(	'Annie',	'Cab',	42);
INSERT	INTO MyBills (Name,Purpose,Amount) VALUES(	'Jack',	'Mobile',	29); 
select * from MyBills;

/*------Task 3-----------*/
create table Students(AdmissionNo Varchar(10), Name VARCHAR(15),	primary	key(AdmissionNo));
INSERT	INTO Students VALUES('2014147',	'James');
INSERT	INTO Students VALUES('2017122',	'Tim');
INSERT	INTO Students VALUES('2017129',	'Annie');
INSERT	INTO Students VALUES('2014147',	'William'); /* Error Code: 1062. Duplicate entry '2014147' for key 'Students.PRIMARY'*/
/*------Task 4-----------*/
CREATE	TABLE	Employees (
EmpId	INTEGER	AUTO_INCREMENT,
Name	VARCHAR(30),
EmpCode VARCHAR(10) UNIQUE,
Email	VARCHAR(30)	UNIQUE,
PRIMARY	KEY	(EmpId)
);

INSERT	INTO Employees (Name,	EmpCode,	Email) VALUES('James',	'FIN145',	'James@Edufect.com');
INSERT	INTO Employees (Name,	EmpCode,	Email) VALUES('Martha',	'TECH366',	'Martha@Edufect.com');
INSERT	INTO Employees (Name,	EmpCode,	Email) VALUES('Julia',	'FIN145',	'Julia@Edufect.com'); /*--Error*/
INSERT INTO Employees (Name,	EmpCode,	Email) VALUES('Martha',	'OPS281',	'Martha@Edufect.com'); /*--ERROR--*/
INSERT INTO Employees (Name,	EmpCode,	Email) VALUES('James',	'OPS043',	'James11@Edufect.com');
select * from Employees;
/*------Task 5-----------*/
CREATE	TABLE Product (
ProdId	INTEGER	AUTO_INCREMENt,
ProdCode INTEGER UNIQUE,
Price INTEGER,
Quantity INTEGER,
PRIMARY	KEY	(ProdId)
);

INSERT	INTO Product (ProdCode,	Price,	Quantity) VALUES(3456,	18,	25);
INSERT	INTO Product (ProdCode,	Price,	Quantity) VALUES(1290,	18,	30);
INSERT	INTO Product (ProdCode,	Price,	Quantity) VALUES(1044,	6,	25);
INSERT	INTO Product (ProdCode,	Price,	Quantity) VALUES(3456,	7,	36);/*Error*/
INSERT	INTO Product (ProdCode,	Price,	Quantity) VALUES(1044,	3,	11);/*Error*/