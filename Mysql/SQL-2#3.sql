/*-----Task 1----*/
select * From Bill;
SELECT name, count(*) From Bill Group by name;
Select name,Sum(Amount) From Bill  Group by name;
Select name, count(*) ,Sum(Amount) From Bill  Group by name;
select Purpose, count(*) From Bill Group by name;
select Purpose, Sum(Amount) From Bill  Group by name;
Select Purpose, count(*) ,Sum(Amount) From Bill  Group by name;
Select name,Purpose, count(*) ,Sum(Amount) From Bill  Group by name;
Select name, max(Amount) From Bill  Group by name;
Select Purpose, max(Amount) From Bill  Group by name;

/*-----Task 2----*/
select * From Products;
SELECT Category, count(*) From Products Group by Category;
SELECT Category, sum(quantity) From Products Group by Category;
SELECT Category, sum(Price*quantity) From Products Group by Category;
SELECT Category, sum(Price*quantity), sum(quantity) From Products Group by Category;
SELECT Category, max(Price), max(quantity) From Products Group by Category;

/*-----Task 3----*/
drop table  StudentExams;
commit;
Create Table StudentExams (	Id	integer, Name Varchar(15),	Subject Varchar(15), Marks Integer);
INSERT INTO StudentExams VALUES(1,	'John',	'English',	63);
INSERT INTO StudentExams VALUES(2,	'John',	'Maths',	70);
INSERT INTO StudentExams VALUES(3,	'John',	'Science',	55);
INSERT INTO StudentExams VALUES(4,	'John',	'Computers', 72);
INSERT INTO StudentExams VALUES(5,	'Tim',	'English',	71);
INSERT INTO StudentExams VALUES(6,	'Tim',	'Maths',	66);
INSERT INTO StudentExams VALUES(7,	'Tim',	'Science',	61);
INSERT INTO StudentExams VALUES(8,	'Tim',	'Computers', 53);
INSERT INTO StudentExams VALUES(9,	'Edwards',	'English',	74);
INSERT INTO StudentExams VALUES(10,	'Edwards',	'Maths',	76);
INSERT INTO StudentExams VALUES(11,	'Edwards',	'Science',	79);
INSERT INTO StudentExams VALUES(12,	'Edwards',	'Computers', 82);
INSERT INTO StudentExams VALUES(13,	'Michael',	'English',	51);
INSERT INTO StudentExams VALUES(14,	'Michael',	'Maths',	48);
INSERT INTO StudentExams VALUES(15,	'Michael',	'Science',	56);
INSERT INTO StudentExams VALUES(16,	'Michael',	'Computers', 46);
INSERT INTO StudentExams VALUES(17,	'Sophia',	'English',	66);
INSERT INTO StudentExams VALUES(18,	'Sophia',	'Maths',	72);
INSERT INTO StudentExams VALUES(19,	'Sophia',	'Science',	68);
INSERT INTO StudentExams VALUES(20,	'Sophia',	'Computers', 73);
commit;
select * from StudentExams;
select name,sum(Marks)  From StudentExams Group by name;
select Subject, count(name) From StudentExams Group by Subject;