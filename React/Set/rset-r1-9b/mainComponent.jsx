import React, { Component } from "react";
import DisplayTable from "./displayTable";
import { getQuestions } from "./questionData";
import QuestionPaper from "./questionPaper";
class MainComponent extends Component {
  state = {
    questionList: getQuestions(),
    view: 0,
    currentQuestionPaper: 0,
    currentQuestion: 0,
    checkCurrentAnswerSheet: false,
  };

  render() {
    return <React.Fragment>{this.renderComponentData()}</React.Fragment>;
  }

  showPaper = (index) => {
    const questionList = [...this.state.questionList];
    const currentQuestionsList = questionList[index];
    currentQuestionsList.questions.map((question) => {
      if (question.choseOption) {
        question.choseOption = undefined;
      }
    });
    questionList[index] = currentQuestionsList;
    console.log(currentQuestionsList);
    this.setState({
      view: 1,
      currentQuestionPaper: index,
      currentQuestion: 0,
      questionList,
      checkCurrentAnswerSheet: false,
    });
  };
  checkAnswerSheet = (index) => {
    this.setState({
      checkCurrentAnswerSheet: true,
      view: 1,
      currentQuestion: 0,
      currentQuestionPaper: index,
    });
  };
  renderComponentData() {
    if (this.state.view === 0) {
      return (
        <DisplayTable
          questionList={this.state.questionList}
          showQuestionPaper={this.showPaper}
          onCheckAnswerSheet={this.checkAnswerSheet}
        />
      );
    } else {
      return (
        <QuestionPaper
          questionPaper={
            this.state.questionList[this.state.currentQuestionPaper]
          }
          questionPaperIndex={this.state.currentQuestionPaper}
          currentQuestion={this.state.currentQuestion}
          onChoseOption={this.handleOption}
          onNextClick={this.next}
          onPreviousClick={this.previous}
          onViewMarksheet={this.viewMarksheet}
          onUpdateQuestion={this.updateQuestion}
          onSumbitAssignment={this.handleAssignment}
          checkCurrentAnswerSheet={this.state.checkCurrentAnswerSheet}
        />
      );
    }
  }
  handleAssignment = () => {
    const questionList = [...this.state.questionList];
    let currentQuestionPaper = questionList[this.state.currentQuestionPaper];
    currentQuestionPaper.isAttemped = true;
    questionList[this.state.currentQuestionPaper] = currentQuestionPaper;
    console.log(currentQuestionPaper);
    this.setState({ view: 0, questionList });
  };

  updateQuestion = (index) => {
    this.setState({ currentQuestion: index });
  };
  next = () => {
    let currentQuestion = this.state.currentQuestion;
    currentQuestion = currentQuestion + 1;
    if (
      currentQuestion <
      this.state.questionList[this.state.currentQuestionPaper].questions.length
    ) {
      this.setState({ currentQuestion });
    }
  };

  previous = () => {
    let currentQuestion = this.state.currentQuestion;
    currentQuestion = currentQuestion - 1;
    if (currentQuestion > -1) {
      this.setState({ currentQuestion });
    }
  };
  handleOption = (option) => {
    console.log(option);
    const questionList = [...this.state.questionList];
    let currentQuestion =
      questionList[this.state.currentQuestionPaper].questions[
        this.state.currentQuestion
      ];
    currentQuestion.choseOption = option;
    questionList[this.state.currentQuestionPaper].questions[
      this.state.currentQuestion
    ] = currentQuestion;
    this.setState({ questionList });
  };
}

export default MainComponent;
