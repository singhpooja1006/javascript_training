import React, { Component } from "react";
import QuestionComp from "./questionComp";
import MarkingSheetComp from "./markingSheetComp";

class QuestionPaper extends Component {
  state = { view: 1 };
  render() {
    return (
      <div className="container">
        <div className="row bg-light">
          <div className="col-12 text-center h1">
            {this.props.questionPaper.subject} : Assignment{" "}
            {this.props.questionPaper.name}
          </div>
          <div className="row col-12">
            <h6 className="mr-auto">&nbsp;&nbsp;&nbsp;Time : 5 mins</h6>
            <h6 className="ml-auto">
              Max Score : {this.props.questionPaper.questions.length}
            </h6>
          </div>
        </div>
        {this.renderComBasedOnView()}
      </div>
    );
  }
  renderComBasedOnView() {
    if (this.state.view === 1) {
      return (
        <React.Fragment>
          <div className="row col-12 mt-1">
            <button
              className="ml-auto btn btn-primary"
              onClick={() => this.viewMarksheet()}
            >
              View Marking Sheet
            </button>
          </div>
          <QuestionComp
            key={this.props.questionPaper.questions[this.props.currentQuestion]}
            question={
              this.props.questionPaper.questions[this.props.currentQuestion]
            }
            currentQuestion={this.props.currentQuestion + 1}
            choseOption={this.props.onChoseOption}
            checkCurrentAnswerSheet={this.props.checkCurrentAnswerSheet}
          />
          {this.displayButton()}
        </React.Fragment>
      );
    } else {
      return (
        <MarkingSheetComp
          questionList={this.props.questionPaper.questions}
          onShowQuestion={this.showQuestion}
          onSumbitAssignment={this.props.onSumbitAssignment}
          checkCurrentAnswerSheet={this.props.checkCurrentAnswerSheet}
        />
      );
    }
  }
  showQuestion = (index) => {
    this.setState({ view: 1 });
    this.props.onUpdateQuestion(index);
  };
  displayButton() {
    if (this.props.currentQuestion === 0) {
      return (
        <button
          className="btn btn-primary"
          onClick={() => this.props.onNextClick()}
        >
          Next Question
        </button>
      );
    } else if (
      this.props.currentQuestion ===
      this.props.questionPaper.questions.length - 1
    ) {
      return (
        <button
          className="btn btn-primary m-1"
          onClick={() => this.props.onPreviousClick()}
        >
          Previous Question
        </button>
      );
    } else {
      return (
        <React.Fragment>
          <button
            className="btn btn-primary m-1"
            onClick={() => this.props.onPreviousClick()}
          >
            Previous Question
          </button>
          <button
            className="btn btn-primary"
            onClick={() => this.props.onNextClick()}
          >
            Next Question
          </button>
        </React.Fragment>
      );
    }
  }
  viewMarksheet = () => {
    this.setState({ view: 2 });
  };
}

export default QuestionPaper;
