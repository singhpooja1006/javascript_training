const questionList = [
  {
    subject: "General Knowledge",
    name: "4A",
    questions: [
      {
        text: "What is the capital of India",
        options: ["New Delhi", "London", "Paris", "Tokyo"],
        answer: "A",
      },
      {
        text: "What is the capital of Italy",
        options: ["Berlin", "London", "Rome", "Paris"],
        answer: "C",
      },
      {
        text: "What is the capital of China",
        options: ["Shanghai", "HongKong", "Shenzen", "Beijing"],
        answer: "D",
      },
      {
        text: "What is the capital of Nepal",
        options: ["Tibet", "Kathmandu", "Colombo", "Kabul"],
        answer: "B",
      },
      {
        text: "What is the capital of Iraq",
        options: ["Baghdad", "Dubai", "Riyadh", "Teheran"],
        answer: "A",
      },
      {
        text: "What is the capital of Bangladesh",
        options: ["Teheran", "Kabul", "Colomdo", "Dhaka"],
        answer: "D",
      },
      {
        text: "What is the capital of Sri Lanka",
        options: ["Islamabad", "Colombo", "Maldives", "Dhaka"],
        answer: "B",
      },
      {
        text: "What is the capital of Saudi Arabia",
        options: ["Baghdad", "Dubai", "Riyadh", "Teheran"],
        answer: "A",
      },
      {
        text: "What is the capital of France",
        options: ["London", "New York", "Paris", "Rome"],
        answer: "C",
      },
      {
        text: "What is the capital of Germany",
        options: ["Frankfurt", "Budapest", "Prague", "Berlin"],
        answer: "D",
      },
      {
        text: "What is the capital of Sweden",
        options: ["Helsinki", "Stockholm", "Copenhagen", "Lisbon"],
        answer: "B",
      },
      {
        text: "What is the currency of UK",
        options: ["Dollar", "Mark", "Yen", "Pound"],
        answer: "D",
      },
      {
        text: "What is the height of Mount Everest",
        options: ["9231 m", "8848 m", "8027 m", "8912 m"],
        answer: "B",
      },
      {
        text: "What is the capital of Japan",
        options: ["Beijing", "Osaka", "Kyoto", "Tokyo"],
        answer: "D",
      },
      {
        text: "What is the capital of Egypt",
        options: ["Cairo", "Teheran", "Baghdad", "Dubai"],
        answer: "A",
      },
    ],
  },
  {
    subject: "Maths",
    name: "10C",
    questions: [
      {
        text: "What is 8 * 9",
        options: ["72", "76", "64", "81"],
        answer: "A",
      },
      {
        text: "What is 2*3+4*5",
        options: ["70", "50", "26", "60"],
        answer: "C",
      },
    ],
  },
  {
    subject: "Chemistry",
    name: "7A(i)",
    questions: [
      {
        text: "What is the melting point of ice",
        options: ["0F", "0C", "100C", "100F"],
        answer: "B",
      },
      {
        text: "What is the atomic number of Oxygen",
        options: ["6", "7", "8", "9"],
        answer: "C",
      },
      {
        text: "What is the atomic number of Carbon",
        options: ["6", "7", "8", "9"],
        answer: "A",
      },
      {
        text: "Which of these is an inert element",
        options: ["Flourine", "Suphur", "Nitrogen", "Argon"],
        answer: "D",
      },
      {
        text: "What is 0 Celsius in Fahrenheit",
        options: ["0", "32", "20", "48"],
        answer: "B",
      },
    ],
  },
  {
    subject: "Computers",
    name: "3B",
    questions: [
      {
        text: "How many bytes are there in 1 kilobyte",
        options: ["16", "256", "1024", "4096"],
        answer: "C",
      },
      {
        text: "Who developed ReactJS",
        options: ["Facebook", "Google", "Microsoft", "Apple"],
        answer: "A",
      },
      {
        text: "Angular is supported by ",
        options: ["Facebook", "Google", "Microsoft", "Twitter"],
        answer: "B",
      },
      {
        text: "C# was developed by ",
        options: ["Amazon", "Google", "Microsoft", "Twitter"],
        answer: "C",
      },
      {
        text: "Bootstrap was developed by ",
        options: ["Apple", "Google", "Facebook", "Twitter"],
        answer: "D",
      },
      {
        text: "AWS is provided by ",
        options: ["Apple", "Amazon", "Microsoft", "Google"],
        answer: "B",
      },
      {
        text: "Azure is provided by ",
        options: ["Microsoft", "Amazon", "IBM", "Google"],
        answer: "A",
      },
      {
        text: "Angular is a framework that uses ",
        options: ["Java", "Python", "C#", "Typescript"],
        answer: "D",
      },
    ],
  },
];

export function getQuestions() {
  return questionList;
}
