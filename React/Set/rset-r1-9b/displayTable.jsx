import React, { Component } from "react";
class DisplayTable extends Component {
  state = {};
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-12 text-center h1">Choose the Assignment</div>
        </div>
        <div className="row text-center">
          <div className="col-3 bg-dark text-white">Subject</div>
          <div className="col-2 bg-dark text-white">Assignment</div>
          <div className="col-3 bg-dark text-white">Performance</div>
          <div className="col-2 bg-dark text-white"></div>
          <div className="col-2 bg-dark text-white"></div>
        </div>
        {this.renderDisplayTable()}
      </div>
    );
  }
  renderDisplayTable() {
    return (
      <React.Fragment>
        {this.props.questionList.map((question, index) => (
          <div className="row text-center border">
            <div className="col-3">{question.subject}</div>
            <div className="col-2">{question.name}</div>
            <div className="col-3">{this.showMarks(question)}</div>
            <div className="col-2">
              <button
                className="btn btn-primary m-1"
                onClick={() => this.props.showQuestionPaper(index)}
              >
                Do
              </button>
            </div>
            <div className="col-2 ">{this.checkAnswer(question, index)}</div>
          </div>
        ))}
      </React.Fragment>
    );
  }
  checkAnswer(questionPaper, index) {
    if (questionPaper.isAttemped === undefined) {
      return "";
    } else {
      return (
        <button
          className="btn btn-primary m-1"
          onClick={() => this.props.onCheckAnswerSheet(index)}
        >
          Check
        </button>
      );
    }
  }
  showMarks(questionPaper) {
    if (questionPaper.isAttemped === undefined) {
      return "No Data";
    } else {
      console.log(questionPaper.questions);
      let totalCount = questionPaper.questions.reduce(this.totalMarks, 0);

      return totalCount + "/" + questionPaper.questions.length;
    }
  }
  totalMarks(acc, curr) {
    if (curr.choseOption === undefined) {
      acc = acc + 0;
      return acc;
    } else if (curr.answer === curr.choseOption) {
      acc = acc + 1;
      return acc;
    } else {
      acc = acc + 0;
      return acc;
    }
  }
}

export default DisplayTable;
