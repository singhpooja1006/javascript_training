import React, { Component } from "react";
class QuestionComp extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-12 ">
            <h3>Question Number : {this.props.currentQuestion}</h3>
            <h6>{this.props.question.text}</h6>
          </div>
        </div>
        {this.renderOptions()}
      </React.Fragment>
    );
  }
  renderOptions() {
    return (
      <div className="row">
        {this.renderOption(this.props.question.options)}
        <div className="col-12 mb-1">
          Your Answer : {this.renderSelectedAns()}
        </div>
        <div className="col-12 mb-1">{this.showCurrentAnswer()}</div>
      </div>
    );
  }
  renderSelectedAns() {
    if (this.props.question.choseOption === undefined) {
      return "Not Answered";
    } else {
      return this.props.question.choseOption;
    }
  }
  showCurrentAnswer() {
    if (this.props.checkCurrentAnswerSheet === true) {
      if (
        this.props.question.choseOption !== undefined &&
        this.props.question.choseOption === this.props.question.answer
      ) {
        return <h6 className="text-success">Correct Answer</h6>;
      } else {
        return (
          <h6 className="text-danger">
            Incorrect. The correct answer is {this.props.question.answer}
          </h6>
        );
      }
    }
  }
  renderOption(options) {
    let optionStyle = ["A", "B", "C", "D"];
    if (this.props.checkCurrentAnswerSheet === true) {
      return (
        <React.Fragment>
          <ol type="A" className="h6">
            {options.map((option, index) => (
              <li
                key={optionStyle[index]}
                value={optionStyle[index]}
                name={option}
              >
                {option}
              </li>
            ))}
          </ol>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <ol type="A" className="h6">
            {options.map((option, index) => (
              <li
                key={optionStyle[index]}
                value={optionStyle[index]}
                name={option}
                style={{ cursor: "pointer" }}
                onClick={() => this.props.choseOption(optionStyle[index])}
              >
                {option}
              </li>
            ))}
          </ol>
        </React.Fragment>
      );
    }
  }
}

export default QuestionComp;
