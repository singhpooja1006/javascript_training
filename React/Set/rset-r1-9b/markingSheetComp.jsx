import React, { Component } from "react";
class MarkingSheetComp extends Component {
  state = {};
  render() {
    return this.renderAnsView(this.props.questionList);
  }

  renderAnsView(questionList) {
    if (this.props.checkCurrentAnswerSheet === false) {
      return (
        <div className="row">
          {questionList.map((question, index) =>
            question.choseOption === undefined ? (
              <div className="col-1">
                <button
                  className="btn btn-warning mt-1"
                  onClick={() => this.props.onShowQuestion(index)}
                >
                  {index + 1 + "."}
                </button>
              </div>
            ) : (
              <div className="col-1">
                <button
                  className="btn btn-secondary mt-1"
                  onClick={() => this.props.onShowQuestion(index)}
                >
                  {index + 1 + "." + question.choseOption}
                </button>
              </div>
            )
          )}
          <div className="col-12 text-center">
            <button
              className="btn btn-secondary mt-5 btn-lg"
              onClick={() => this.props.onSumbitAssignment()}
            >
              Submit the Assignment
            </button>
          </div>
        </div>
      );
    } else {
      return (
        <div className="row">
          {questionList.map((question, index) =>
            question.choseOption === undefined ? (
              <div className="col-1">
                <button
                  className="btn btn-warning mt-1"
                  onClick={() => this.props.onShowQuestion(index)}
                >
                  {index + 1 + "."}
                </button>
              </div>
            ) : question.choseOption === question.answer ? (
              <div className="col-1">
                <button
                  className="btn btn-success mt-1"
                  onClick={() => this.props.onShowQuestion(index)}
                >
                  {index + 1 + "." + question.choseOption}
                </button>
              </div>
            ) : (
              <div className="col-1">
                <button
                  className="btn btn-danger mt-1"
                  onClick={() => this.props.onShowQuestion(index)}
                >
                  {index + 1 + "." + question.choseOption}
                </button>
              </div>
            )
          )}
          <div className="col-12 text-center">
            <button
              className="btn btn-secondary mt-5 btn-lg"
              onClick={() => this.props.onSumbitAssignment()}
            >
              Go to the List of Assignments
            </button>
          </div>
        </div>
      );
    }
  }
}

export default MarkingSheetComp;
